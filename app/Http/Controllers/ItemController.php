<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    public function get_item($id) {
  


        $qt = DB::select(DB::raw("
        SELECT * FROM item LEFT JOIN quotation ON quotation.id = item.item_quotation_id WHERE quotation.id= ".$id."
        "));

        foreach ($qt as $row) {

            $original_date = $row->issueDate;
            // Creating timestamp from given date
            $timestamp = strtotime($original_date);
            // Creating new date format from that timestamp
            $new_date = date("d-m-Y", $timestamp);
            $row->issueDate = $new_date;

        }

        echo json_encode($qt);
   }
}
