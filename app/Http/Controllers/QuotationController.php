<?php

namespace App\Http\Controllers;
use Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class QuotationController extends Controller
{
    //
    public function index() {
    
        // $users = DB::table('quotation')
        //         ->where('id_customer',1)
        //         ->get();

        $id = 1;

        $qt = DB::select(DB::raw("
            
            SELECT * FROM quotation as qt
            LEFT JOIN customer as ct ON qt.quotation_customer_id = ct.customer_id 
        
        "));

        foreach ($qt as $row) {

            $original_date = $row->issue_date;
            // Creating timestamp from given date
            $timestamp = strtotime($original_date);
            // Creating new date format from that timestamp
            $new_date = date("d-m-Y", $timestamp);
            $row->issue_date = $new_date;

        }

        return response($qt);
   }

   public function index2(Request $request) {

        $data = json_decode($request->getContent(),true);
        
        echo ($data['expireDate']);
        echo ($data['salemanName']);
        

        echo ($data['amphoe']);
   }

    public function get_quotation() {

    }

    public function insert_quotation(Request $request) {
    
        $data = json_decode($request->getContent(),true);

        $customer_id = DB::table('customer')->insertGetId([
            'name' => $data['customerName'],
            'email' => $data['customerEmail'],
            'phone_no' => $data['customerTele'],
            'district' => $data['district'],
            'province' => $data['province'],
            'amphoe' => $data['amphoe'],
            'zipcode' => $data['zipcode'],
            'address' => $data['customerBuilding'] 
        ]);
        
        $quotation_id = DB::table('quotation')->insertGetId([
            'issue_date' => $data['issueDate'],
            'expire_date' => $data['expireDate'],
            'delivery_date' => $data['deliveryDate'],
            'saleman_name' => $data['salemanName'],
            'phone_no' => $data['phoneNumber'],
            'credit' => $data['credit'],
            'vat_type' => $data['vatType'],
            'vat_amt' => $data['vatValue'],
            'discount_type' => $data['type'],
            'discount_amt' => $data['discountAll'],
            'sub_total' => $data['total'],
            'total' => $data['netTotal'],
            'quotation_customer_id' => $customer_id,
        ]);

        foreach($data['itemList'] as $row ) {
            DB::table('item')->insert([
                'name' => $row['name'],
                'amount' => $row['amount'],
                'price' => $row['price'],
                'discount' => $row['discount'],
                'sum' => $row['sum'],
                'item_quotation_id' => $quotation_id,
            ]);
        }

        $response['id'] = $quotation_id;
        return response($response);
    }

    function quotation_detail($id) {
        
        $qt = DB::select(DB::raw("
            
            
            SELECT * FROM quotation as qt
            LEFT JOIN customer as ct ON qt.id_customer = ct.id_customer 
            WHERE qt.id_customer = ".$id."
        
        "));

        foreach ($qt as $row) {

            $original_date = $row->issueDate;
            // Creating timestamp from given date
            $timestamp = strtotime($original_date);
            // Creating new date format from that timestamp
            $new_date = date("d-m-Y", $timestamp);
            $row->issueDate = $new_date;

            $item = DB::select(DB::raw("
            
                SELECT * FROM item as it
                WHERE it.item_quotation_id = ".$id."
            
            "));

            $row->item = $item; 

        }

        echo json_encode($qt);
    }

    function get_customer($id) {
        
        $qt = DB::select(DB::raw("
            
            SELECT * FROM customer where id_customer=".$id."
        
        "));

        // foreach ($qt as $row) {

        //     $original_date = $row->issueDate;
        //     // Creating timestamp from given date
        //     $timestamp = strtotime($original_date);
        //     // Creating new date format from that timestamp
        //     $new_date = date("d-m-Y", $timestamp);
        //     $row->issueDate = $new_date;
        // }

        echo json_encode($qt);
    }
}
