<?php

namespace App\Http\Controllers;

use Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
// SELECT * FROM quotation LEFT JOIN user ON quotation.id = user.id_user WHERE user.id_user=id
// SELECT * FROM user LEFT JOIN quotation ON quotation.id = user.id_user WHERE user.id_user=id
class UserController extends Controller
{

    public function insert_user(Request $request)
    {
        $data = json_decode($request->getContent(),true);
        $user_id = DB::table('customer')->insertGetId([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'username' => $data['username'],
            'password' => $data['password'],
            'email' => $data['email'],
            'position' => $data['position'],
            'phoneNumber' => $data['phoneNumber'],
        ]);
        echo($data);
        return response($user_id);
    }

    public function get_User($id)
    {
        $qt = DB::select(DB::raw("
    SELECT * FROM `user` WHERE `id_user`=$id
    "));

        // foreach ($qt as $row) {

        //     $original_date = $row->issueDate;
        //     // Creating timestamp from given date
        //     $timestamp = strtotime($original_date);
        //     // Creating new date format from that timestamp
        //     $new_date = date("d-m-Y", $timestamp);
        //     $row->issueDate = $new_date;

        // }

        echo json_encode($qt);
    }
    public function delete_user($id)
    {
        $id_user = 2;
        $name = "kawisara";
        $lastname = "Aumkhun";
        $position = "accounting";
        $email = "ppp@hotmail.com";
        $status = "open";
        $username = "pai";
        $password = "1234";
        $qt = DB::select(DB::raw("
        DELETE FROM `user` 
        WHERE `id_user`=$id
        "));
        echo json_encode($qt);    
   
    }

    public function update_user()
    {
        $id_user = 2;
        $name = "kawisara";
        $lastname = "Aumkhun";
        $position = "accounting";
        $email = "ppp@hotmail.com";
        $status = "open";
        $username = "pai";
        $password = "1234";
        $qt = DB::select(DB::raw("
        UPDATE user SET name=$name, 
            lastname=$lastname, 
            position=$position,
            email=$email, 
            status=$status, 
            username=$username, 
            password=$password   
        WHERE id_user=$id_user
        "));
        echo json_encode($qt);
    }

    public function get_quotation_from_UserId()
    {


        $id = 1;

        $qt = DB::select(DB::raw("
        SELECT * FROM quotation LEFT JOIN user ON quotation.id = user.id_user WHERE user.id_user=$id
        "));

        foreach ($qt as $row) {

            $original_date = $row->issueDate;
            // Creating timestamp from given date
            $timestamp = strtotime($original_date);
            // Creating new date format from that timestamp
            $new_date = date("d-m-Y", $timestamp);
            $row->issueDate = $new_date;
        }

        echo json_encode($qt);
    }
}
